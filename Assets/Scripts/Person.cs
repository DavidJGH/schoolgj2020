﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Enums;
using Sirenix.OdinInspector;
using UnityEngine;

public class Person : SerializedMonoBehaviour
{
    public string firstName;
    public string lastName;
    //true: female, false: male
    public bool gender;
    public DateTime birthdate;
    public Countries country;
    public DateTime expiration;
    public string description;

    public List<Flags> flags = new List<Flags>();
    
    public void SetAttributes(PersonInfo info)
    {
        firstName = info.firstName;
        lastName = info.lastName;
        if(info.gender != null)
            gender = (bool)info.gender;
        if(info.birthdate != null)
            birthdate = (DateTime)info.birthdate;
        if(info.country != null)
            country = (Countries)info.country;
        if (info.expiration != null)
            expiration = (DateTime) info.expiration;
        flags.AddRange(info.flags);
    }
}

public class PersonInfo
{
    public string firstName;
    public string lastName;
    public bool? gender;
    public DateTime? birthdate;
    public Countries? country;
    public DateTime? expiration;

    public List<Flags> flags = new List<Flags>();

    public PersonInfo()
    {
    }
    
    public PersonInfo(string firstName, string lastName, bool gender, DateTime birthdate, Countries country, DateTime? expiration, params Flags[] hasFlags)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.birthdate = birthdate;
        this.country = country;
        this.expiration = expiration;
        flags = new List<Flags>();
        flags.AddRange(hasFlags);
    }
}

public enum Flags
{
    Infected,
    Target,
    hasGoodLuggage,
    hasContaminatedLuggage,
    InvalidPassport
}