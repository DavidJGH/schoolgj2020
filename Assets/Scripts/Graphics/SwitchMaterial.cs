﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Renderer))]
public class SwitchMaterial : MonoBehaviour {
    private Renderer _renderer;
    [SerializeField] private Material mat1;
    [SerializeField] private Material mat2;

    [SerializeField] private int index;
    
    private bool hasOne;

    private void Awake() {
        _renderer = GetComponent<Renderer>();
    }

    public void SwitchToOne() {
        var mats = _renderer.materials;
        mats[index] = mat1;
        _renderer.materials = mats;
    }
    
    public void SwitchToTwo() {
        var mats = _renderer.materials;
        mats[index] = mat2;
        _renderer.materials = mats;
    }

    public void Toggle() {
        if (hasOne) {
            _renderer.materials[index] = mat2;
        } else {
            _renderer.materials[index] = mat1;

        }
    }
}
