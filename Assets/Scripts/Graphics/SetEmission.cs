﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

[RequireComponent(typeof(Renderer))]
public class SetEmission : MonoBehaviour {
    private Renderer renderer;

    [SerializeField]
    private int index;

    [SerializeField] [ColorUsage(true, true)]
    public Color color;

    private static readonly int EmissionColor = Shader.PropertyToID("_EmissionColor");

    private void Awake() {
        if (renderer == null) {
            renderer = GetComponent<Renderer>();
        }
    }

    public void SetEmissive() {
        DOTween.To(() => renderer.materials[index].GetColor(EmissionColor),
            value => renderer.materials[index].SetColor(EmissionColor, value), color, 0.2f);
    }

    public void SetUnemissive() {
        DOTween.To(() => renderer.materials[index].GetColor(EmissionColor),
            value => renderer.materials[index].SetColor(EmissionColor, value), Color.black, 0.2f);
    }
}