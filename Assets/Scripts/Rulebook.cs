﻿using Enums;
using Rules;
using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class Rulebook : SerializedMonoBehaviour {
    public static Rulebook instance;

    public UnityEvent onRulesChanged;

    private void Awake() {
        if (instance == null) {
            instance = this;
        }
    }

    public List<Rule> activeRules = new List<Rule>();
    public List<Rule> targetRules = new List<Rule>();
    public List<Rule> infectedRules = new List<Rule>();

    //Returns true when person is to be sorted out
    public bool CheckPersonMayPass(Person person, ref List<Rule> rulesThatWereApplied, ref List<Flags> flagsThatWereApllied) {
        
        AddRulesAndFlagsToLists(person, ref rulesThatWereApplied, ref flagsThatWereApllied);
        //AddRulesAndFlagsToLists(person, targetRules, ref rulesThatWereApplied, ref flagsThatWereApllied);
        //AddRulesAndFlagsToLists(person, infectedRules, ref rulesThatWereApplied, ref flagsThatWereApllied);
        return !activeRules.Any(rule => rule.AppliesTo(person)) && !targetRules.Any(rule => rule.AppliesTo(person)) &&
               !infectedRules.Any(rule => rule.AppliesTo(person));
    }
 
    public bool CheckPersonIsDenied(Person person, ref List<Rule> rulesThatWereApplied, ref List<Flags> flagsThatWereApllied)
    {
        AddRulesAndFlagsToLists(person, ref rulesThatWereApplied, ref flagsThatWereApllied);
        return activeRules.Any(rule => rule.AppliesTo(person)) && !targetRules.Any(rule => rule.AppliesTo(person)) &&
               !infectedRules.Any(rule => rule.AppliesTo(person));
    }

    public bool CheckPersonIsTarget(Person person, ref List<Rule> rulesThatWereApplied, ref List<Flags> flagsThatWereApllied)
    {
        AddRulesAndFlagsToLists(person, ref rulesThatWereApplied, ref flagsThatWereApllied);
        return targetRules.Any(rule => rule.AppliesTo(person));
    }

    public bool CheckPersonIsInfected(Person person, ref List<Rule> rulesThatWereApplied, ref List<Flags> flagsThatWereApllied)
    {
        AddRulesAndFlagsToLists(person, ref rulesThatWereApplied, ref flagsThatWereApllied);
        return infectedRules.Any(rule => rule.AppliesTo(person)) && !targetRules.Any(rule => rule.AppliesTo(person));
    }

    public void AddRulesAndFlagsToLists(Person checkedPerson, ref List<Rule> rulesThatWereApplied, ref List<Flags> flagsThatWereApllied)
    {
        foreach (Rule rule in activeRules)
        {
            if (rule.AppliesTo(checkedPerson))
            {

                if (rulesThatWereApplied.Any(r => r is FlagRule) && rule is FlagRule)
                {
                    //there is already a flagrule and this is a flagrule
                }
                else
                {
                    rulesThatWereApplied.Add(rule);
                }
            }
            if (rule is FlagRule)
            {
                foreach (Flags flag in (Flags[])Enum.GetValues(typeof(Flags)))
                {
                    if (checkedPerson.flags.Contains(flag))
                    {
                        if (!(flag is Flags.hasGoodLuggage)){
                            flagsThatWereApllied.Add(flag);
                        }
                    }
                }
            }
        }
    }

    public void newRuleSet(int day) {
        if (day == 1) {
            //this is to test the system
            ImplementStandartRuleSet();
        } else if (day >= 2 && day <= 4) {
            //Add either AgeRule or CountryRule
            AddRandomRule();
        } else if (day >= 5) {
            //change an existing rule except for flagRule and tempRule
            ChangeCountryRule();
            /*
            int ruleToChange = Random.Range(0, 2);
            if(ruleToChange == 0)
            {
                ChangeCountryRule();
            }
            else if(ruleToChange == 1)
            {
                //force change age rule
                SetAgeRule(true);
            }*/
        }
    }

    void ImplementStandartRuleSet() {
        FlagRule standardFlags = new FlagRule();
        standardFlags.flags.Add(Flags.hasContaminatedLuggage);
        standardFlags.flags.Add(Flags.InvalidPassport);
        FlagRule infectedFlags = new FlagRule();
        infectedFlags.flags.Add(Flags.Infected);
        FlagRule targetFlags = new FlagRule();
        targetFlags.flags.Add(Flags.Target);

        AddRule(standardFlags, Ruleset.Standard);
        AddRule(infectedFlags, Ruleset.Infected);
        AddRule(targetFlags, Ruleset.Target);

        ExpiryRule expiryRule = new ExpiryRule();
        AddRule(expiryRule, Ruleset.Standard);
    }

    void AddRandomRule() {
        AddCountryRule();
        /*
        if(rule == 0)
        {
           
            AddCountryRule();
        }
        else if(rule == 1)
        { 
            //age rule
            bool setAnAgeRule = SetAgeRule(false); //dont force change the age
            if (!setAnAgeRule)
            {
                //only setAgeRule once -> if already existing add CountryRule instead
                AddCountryRule();
            }
        }*/
    }

    void AddCountryRule() {
        CountryRule countryRule = new CountryRule();
        foreach (Rule rule in activeRules) {
            if (rule is CountryRule) {
                countryRule = (CountryRule) rule;
            }
        }

        countryRule.countries.Add((Countries)UnityEngine.Random.Range(PersonGenerator.instance.countrySubsetStart,
            PersonGenerator.instance.countrySubsetStart +
            PersonGenerator.instance.countrySubsetCount));
        AddRule(countryRule, Ruleset.Standard);
    }

    void AddRule(Rule rule, Ruleset ruleset) {
        switch (ruleset) {
            case Ruleset.Standard:
                activeRules.Add(rule);
                break;
            case Ruleset.Infected:
                infectedRules.Add(rule);
                break;
            case Ruleset.Target:
                targetRules.Add(rule);
                break;
        }

        onRulesChanged.Invoke();
    }

    bool RemoveRule(Rule rule, Ruleset ruleset) {
        bool remove = false;

        switch (ruleset) {
            case Ruleset.Standard:
                remove = activeRules.Remove(rule);
                break;
            case Ruleset.Infected:
                remove = infectedRules.Remove(rule);
                break;
            case Ruleset.Target:
                remove = targetRules.Remove(rule);
                break;
        }

        onRulesChanged.Invoke();
        return remove;
    }

    void ChangeCountryRule() {
        //remove a country rule and add a new

        //remove
        CountryRule countryRule = new CountryRule();
        foreach (Rule rule in activeRules) {
            if (rule is CountryRule) {
                countryRule = (CountryRule) rule;
            }
        }

        countryRule.countries.RemoveAt(UnityEngine.Random.Range(0, countryRule.countries.Count));

        //add new
        AddCountryRule();
    }

    /*bool SetAgeRule(bool forceChange)
    {
        bool alreadyAgeRule = false;
        foreach(Rule rule in activeRules)
        {
            if(rule is AgeRule)
            {
                alreadyAgeRule = true;
                if (forceChange)
                {
                    RemoveRule(rule);
                }
            }
        }
        
        if (forceChange || !alreadyAgeRule)
        {
            AgeRule ageRule = new AgeRule();
            ageRule.minAge = Random.Range(18, 27);
            ageRule.maxAge = Random.Range(40, 81);
            AddRule(ageRule);
        }
        return !alreadyAgeRule;
    }*/

    public enum Ruleset {
        Standard,
        Infected,
        Target
    }
}