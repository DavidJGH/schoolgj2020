﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityTemplateProjects;
using UnityEngine.Events;

public class QueueManager : MonoBehaviour
{
    public static QueueManager instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    public List<GameObject> queuedPeople = new List<GameObject>();
    public List<TargetQueueInfo> targetNotificationTiming = new List<TargetQueueInfo>();
    public GameObject activePersonObject;
    public Person activePerson;
    public float distanceBetweenPeople;
    public UnityEvent OnQueueDone;
    public UnityEvent OnNewActivePerson;
    public GameObject[] queuedPeopleArrayMovement;
    public Transform QueueSpawnPoint;
    public Transform YesExit;
    public Transform NoExit;
    public Transform TargetExit;
    public Decision decision;
    public int queuePosition;
    public GameObject lastPersonThatWentThrough;
    public List<GameObject> pplThatWentThrough;
    public UnityEvent TargetNotification;
    public GameObject[] queuedPeopleArray;


    public float delay;

    // Start is called before the firsat frame update
    void Start()
    {
    }

    public void NextPerson()
    {
        NextPersonInLine();
    }

    public int numPeopleLeftInQueue()
    {
        return queuedPeople.Count - queuePosition + 1;
    }

    void SendNotificationAboutTarget(Person target)
    {
        //BLABLALBALBLBA TARGET BLALBALBLABLA
        Debug.Log("Be careful there is the target " + target.firstName + " coming up!");
        TargetNotification.Invoke();
    }

    public void timeIsUp()
    {
        Debug.Log("Move ppl back");
        //move people back to spawn point
      
        foreach(GameObject person in queuedPeople)
        {
            //MovePeopleBack(person);
            StartCoroutine(MovePeopleBack(person));
            //queuedPeople.Remove(person);
        }
        StartCoroutine(MovePeopleBack(activePersonObject));

        //wait some time
        //queuedPeople = new List<GameObject>();
        //queuedPeopleArray = queuedPeople.ToArray();
        queuedPeople.Clear();
        queuedPeopleArray = queuedPeople.ToArray();

        //start next day
        activePersonObject = null;
        activePerson = null;
        StartCoroutine(WaitTime(3));
        OnQueueDone.Invoke();
    }

    IEnumerator WaitTime(int sec)
    {
        WaitForSeconds wait = new WaitForSeconds(sec);
        yield return wait;
    }

    //is called every time you click on y/n 
    void NextPersonInLine()
    {
        queuePosition++;
        int notificationBuffer = Random.Range(1, 5);
        foreach(TargetQueueInfo tqi in targetNotificationTiming)
        {
            if(queuePosition + notificationBuffer == tqi.queuePosition)
            {
                tqi.wasNotified = true;
                SendNotificationAboutTarget(tqi.targetPerson);
            }
            else if(tqi.queuePosition < queuePosition + notificationBuffer && !tqi.wasNotified)
            {
                tqi.wasNotified = true;
                SendNotificationAboutTarget(tqi.targetPerson);
            }
        }
        List<GameObject> newQueuedPeopleList = new List<GameObject>();
        queuedPeopleArray = queuedPeople.ToArray();
        queuedPeopleArrayMovement = queuedPeople.ToArray();
        for(int i = 0; i < queuedPeopleArray.Length; i++)
        {
            //go through all people in the queue

            if(i == 0){
                //is the next person in queue

                //set his Gameobject to active
                if (activePersonObject != null)
                {
                    //move him to yes or no position
                    Transform positionOfPerson = activePersonObject.GetComponent<Transform>();
                    Vector3 goal = new Vector3();
                    if (decision is Decision.LetPass)
                    {
                        goal = YesExit.position;
                    }
                    else if(decision is Decision.DenyEntry)
                    {
                        goal = NoExit.position;
                    }
                    else if(decision is Decision.IsTarget || decision is Decision.IsInfected)
                    {
                        goal = TargetExit.position;
                    }
                    positionOfPerson.DOMove(goal, Vector3.Distance(goal, positionOfPerson.position) / 2)
                        .OnComplete(() =>
                        {
                                foreach(GameObject person in pplThatWentThrough)
                                {
                                    pplThatWentThrough.Remove(person);
                                    //Destroy(person);
                                }
                            
                        });
                    
                }

                if(activePersonObject != null)
                {
                    lastPersonThatWentThrough = activePersonObject;
                    pplThatWentThrough.Add(activePersonObject);
                }

                activePersonObject = queuedPeopleArray[i];

                //set the player to active
                activePerson = activePersonObject.GetComponent<Person>();
                OnNewActivePerson.Invoke();
                
               /* GameObject activePassportPhoto = activePersonObject;
                activePassportPhoto.transform.position = PassportPhotoManager.instance.passportPhotoPosition.position;*/
            }
            
            //move people forwards in the array and cast it into a list 
            if (i != queuedPeopleArray.Length - 1)
            {
                queuedPeopleArray[i] = queuedPeopleArray[i + 1];
            }

            if (i == queuedPeopleArray.Length - 1)
            {
                //is the last person in queue
                
                //delete entry
                queuedPeopleArray[i] = null;
            }
            if (queuedPeopleArray[i] != null)
            {
                newQueuedPeopleList.Add(queuedPeopleArray[i]);
            }
            //add delay between the movement of people
        }
        StartCoroutine(Movement());
        queuedPeople = newQueuedPeopleList;
        if (queuedPeopleArray.Length == 0 && activePersonObject != null)
        {
            //deactivate last person
            //activePersonObject.SetActive(false);

            //TODO: play animation to go to next day

            //go to next day
            OnQueueDone.Invoke();
        }
        
    }

    IEnumerator Movement()
    {
        WaitForSeconds wait = new WaitForSeconds(delay);
        for (int i = 0; i < queuedPeopleArrayMovement.Length; i++)
        {
            //move person 
            //play movement animation 
            Transform positionOfPerson = queuedPeopleArrayMovement[i].GetComponent<Transform>();
            Vector3 goal = new Vector3(distanceBetweenPeople * i + 1,
                                                    positionOfPerson.position.y,
                                                    positionOfPerson.position.z);
            positionOfPerson.DOMove(goal, Vector3.Distance(goal, positionOfPerson.position) / 2);

            yield return wait;
        }
    } 

    IEnumerator MovePeopleBack(GameObject person)
    {
        WaitForSeconds wait = new WaitForSeconds(delay);
        Transform positionOfPerson = person.GetComponent<Transform>();
        Vector3 goal = QueueSpawnPoint.position;

        //TODO: here it has to stop the other tweener movements
        positionOfPerson.DOMove(goal, Vector3.Distance(goal, positionOfPerson.position) / 2).OnComplete(() => Destroy(person));

        yield return wait;
    }
}

public class TargetQueueInfo
{

    public Person targetPerson;
    public int queuePosition;
    public bool wasNotified;
    //Add bool if was triggered and check when person becomes active if notification was sent out already
    public TargetQueueInfo(Person target, int pos)
    {
        targetPerson = target;
        queuePosition = pos;
        wasNotified = false;
    }
}

