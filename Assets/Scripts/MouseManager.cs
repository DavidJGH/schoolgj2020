using System;
using Interactables;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.EventSystems;

public class MouseManager : MonoBehaviour {
    public static MouseManager instance;
    private Camera camera;

    [ReadOnly]
    public Vector3 hitLocation;

    [ReadOnly]
    public GameObject hitGameObject;

    [ReadOnly]
    public GameObject currentlySelectedEventSystemGameObject;

    [ReadOnly]
    public bool hitSomethingThisFrame;

    [SerializeField]
    private LayerMask layerMask;

    private StandaloneInputModuleV2 inputModule;

    private void Awake() {
        if (instance == null) {
            instance = this;
        }

        camera = Camera.main;
    }
    
    private void Update() {
        Ray ray = camera.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out var hit, 100f, layerMask)) {
            hitSomethingThisFrame = true;
            hitLocation = hit.point;
            hitGameObject = hit.collider.gameObject;
        } else {
            hitSomethingThisFrame = false;
        }

        if (inputModule == null) {
            inputModule = EventSystem.current.currentInputModule as StandaloneInputModuleV2;
        }
        
        if (inputModule != null) {
            currentlySelectedEventSystemGameObject = inputModule.GameObjectUnderPointer();
        }
    }
}