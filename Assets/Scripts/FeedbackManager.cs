﻿using Rules;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeedbackManager : MonoBehaviour
{
    public static FeedbackManager instance;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GiveFeedback(List<Rule> rulesThatWereApplied, List<Flags> flagsThatWereApplied)
    {
        //TODO: Exclude Feedback from the decision you made: example pressed virus but it is wrong ->
        //shouldnt tell you that he had a virus but only that he was also a target and you missed that

        //doesnt show infected
        foreach(Rule rule in rulesThatWereApplied)
        {
            if (rule is FlagRule)
            {
                foreach (Flags flag in flagsThatWereApplied)
                {
                    Debug.Log("PROBLEM: Flag applied: " + flag.ToString());
                    
                }
            }
            else
            {
                Debug.Log("PROBLEM: Rule applied: " + rule.GetType().ToString());
            }
        }
    }
}
