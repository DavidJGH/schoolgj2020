﻿using System;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class MusicManager : MonoBehaviour {
    [SerializeField] private List<AudioClip> audioClips;
    private AudioSource audioSource;

    private int activeTrackIndex;
    private AudioClip ActiveClip => audioClips[activeTrackIndex];

    private void Awake() {
        audioSource = GetComponent<AudioSource>();
        activeTrackIndex = 0;
    }

    private void Start() {
        StartPlayingCurrentClip();
    }

    private void AudioClipDone() {
        if (activeTrackIndex < audioClips.Count-1) {
            activeTrackIndex++;
            StartPlayingCurrentClip();
        }
    }

    private void Update() {
        if (ActiveClip.length - audioSource.time < 0.1f) {
            AudioClipDone();
        }
    }

    private bool paused;

    public void TogglePlayback() {
        if (paused) {
            Unpause();
        } else {
            Pause();
        }
    }
    
    public void Pause() {
        paused = true;
        audioSource.Pause();
    }

    public void Unpause() {
        paused = false;
        audioSource.UnPause();
    }

    private void StartPlayingCurrentClip() {
        audioSource.clip = ActiveClip;
        audioSource.Play();
    }
}
