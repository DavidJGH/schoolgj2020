﻿using System.Linq;
using Boo.Lang;
using Enums;

namespace Rules
{
    public class CountryRule : Rule
    {
        public List<Countries> countries = new List<Countries>();
        
        public override bool AppliesTo(Person person)
        {
            return countries.Any(e => person.country.Equals(e));
        }
    }
}