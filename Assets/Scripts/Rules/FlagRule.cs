﻿using System.Collections.Generic;
using System.Linq;

namespace Rules
{
    public class FlagRule : Rule
    {
        public List<Flags> flags = new List<Flags>();
        
        public override bool AppliesTo(Person person)
        {
            return flags.Any(e => person.flags.Any(f => f == e));
        }
    }
}