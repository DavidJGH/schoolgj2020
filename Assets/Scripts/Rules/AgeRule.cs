﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Rules
{
    public class AgeRule : Rule
    {
        public int minAge;
        public int maxAge;

        public override bool AppliesTo(Person person)
        {
            var today = DateTime.Today;
            var age = today.Year - person.birthdate.Year; //birthday 2000 März -> 2020 - 2000 -> check month
            if(today.Month < person.birthdate.Month)
            {
                //didnt have birthday yet this year
                age--;
            }
            else if(today.Month == person.birthdate.Month)
            {
                //birthday is in the same month -> check the day
                if(today.Day < person.birthdate.Month)
                {
                    //birthday coming up soon
                    age--;
                }
            }
            return (age < minAge || age > maxAge);
        }
    }
}