﻿using System;

namespace Rules
{
    public class ExpiryRule : Rule
    {
        public override bool AppliesTo(Person person)
        {
            return person.expiration < DateTime.Today;
        }
    }
}