﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DelayAction : MonoBehaviour {
    public UnityEvent onDoAction;

    private bool doneWithAction;

    public void StartAction(float delay) {
        Invoke(nameof(DoAction), delay);
        doneWithAction = false;
    }

    private void DoAction() {
        if (!doneWithAction) {
            doneWithAction = true;
            onDoAction.Invoke();
        }
    }

}
