﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class Radio : MonoBehaviour {
    private Tweener tweener;
    private void Start() {
        tweener = transform.DOPunchScale(Vector3.one * 0.03f, 0.5f).SetEase(Ease.InOutCirc).SetLoops(-1, LoopType.Incremental);
    }

    public void Toggle() {
        if (tweener.IsPlaying()) {
            tweener.Pause();
        } else {
            tweener.Restart();
        }
    }
}
