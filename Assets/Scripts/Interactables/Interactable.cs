﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;


namespace Interactables {
    public class Interactable : MonoBehaviour {
        public UnityEvent OnClickDown;
        public UnityEvent OnClickUp;

        private void Awake() {
            if (OnClickDown == null) {
                OnClickDown = new UnityEvent();
            }

            if (OnClickUp == null) {
                OnClickUp = new UnityEvent();
            }
        }

        private void OnMouseDown() {
            OnClickDown.Invoke();
        }

        private void OnMouseUpAsButton() {
            OnClickUp.Invoke();
        }
    }
}