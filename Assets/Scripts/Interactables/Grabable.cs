﻿using System;
using System.Collections;
using UnityEngine;
using UnityTemplateProjects;

//[RequireComponent(typeof(Rigidbody))]
public class Grabable : MonoBehaviour {
    private Rigidbody rb;

    [SerializeField] private float lerpPercentage = 0.2f;
    [SerializeField] private float rotateSpeed = 8f;
    [SerializeField] private float height = 0.2f;
    [SerializeField] private Transform centre;

    private Vector3 posOnStart;
    
    private Coroutine _coroutine;
    private bool grabbing;

    private void Awake() {
        rb = GetComponent<Rigidbody>();
    }

    private void Update() {
        if (grabbing && Input.GetMouseButtonUp(0)) {
            EndGrab();
        }
    }

    public void StartGrab() {
        if (this.isActiveAndEnabled) {
            rb.isKinematic = true;
            posOnStart = MouseManager.instance.hitLocation - centre.position;
            _coroutine = StartCoroutine(DragRoutine());
            grabbing = true;
        }
    }

    IEnumerator DragRoutine() {
        float mouseWheel = 0f;
        while (true) {
            mouseWheel = Mathf.Lerp(mouseWheel,Input.mouseScrollDelta.y,0.23f);
            rb.transform.RotateAround(centre.position, Vector3.up, rotateSpeed*mouseWheel);
            
            if (MouseManager.instance.hitSomethingThisFrame) {
                Vector3 targetPos = MouseManager.instance.hitLocation - transform.TransformVector(centre.localPosition) + Vector3.up * height - posOnStart;
                rb.position = Vector3.Lerp(rb.position, targetPos, lerpPercentage);
            }

            yield return new WaitForFixedUpdate();
        }
    }

    public void EndGrab() {
        if (_coroutine != null) {
            grabbing = false;
            rb.isKinematic = false;
            StopCoroutine(_coroutine);
            _coroutine = null;
        }
    }
}