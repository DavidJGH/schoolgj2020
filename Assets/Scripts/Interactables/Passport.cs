﻿using System;
using System.Collections;
using System.Collections.Generic;
using Interactables;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Animator))]
public class Passport : MonoBehaviour {
    private Quaternion upRotation;
    private Quaternion downRotation;

    private Animator animator;
    private static readonly int openID = Animator.StringToHash("Open");
    private static readonly int openRightID = Animator.StringToHash("OpenRight");
    private static readonly int openLeftID = Animator.StringToHash("OpenLeft");

    private bool open;

    private float angleUp;
    private float angleDown;

    [SerializeField] private GameObject rightSide;
    
    public UnityEvent onOpenClick;
    public UnityEvent onClosedClick;

    private void Awake() {
        if (onOpenClick == null) {
            onOpenClick = new UnityEvent();
        }
        if (onClosedClick == null) {
            onClosedClick = new UnityEvent();
        }
        
        if (animator == null) {
            animator = GetComponent<Animator>();
        }
        upRotation = Quaternion.LookRotation(Vector3.down);
        downRotation = Quaternion.LookRotation(Vector3.up);
    }

    public void OnClickDown() {
        if (open) {
            Close();
        } else {
            Open();
        }
    }

    private void Open() {
        open = true;
        
        angleUp = Vector3.SignedAngle(transform.up,Vector3.right, Vector3.forward);
        angleDown = Vector3.SignedAngle(transform.up,Vector3.left, Vector3.forward);
        
        animator.SetBool(openID, true);
        animator.SetTrigger(angleUp>angleDown?openRightID:openLeftID);
    }

    private void Close() {
        open = false;
        animator.SetBool(openID, false);
    }
    
    
    public void OnClick() {
        if (MouseManager.instance.currentlySelectedEventSystemGameObject == rightSide) {
            if (open) {
                onOpenClick.Invoke();
            } else {
                onClosedClick.Invoke();
            }
        } else {
            onClosedClick.Invoke();
        }
    }
}