﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class ClickDurationManger : MonoBehaviour {
    [SerializeField] private float longClickTime = 0.2f;
    
    public UnityEvent shortClickDown;
    public UnityEvent longClickDown;
    public UnityEvent longClickUp;


    private float timeClickStarted;
    private bool clicked;

    private void Awake() {
        if (shortClickDown == null) {
            shortClickDown = new UnityEvent();
        }
        if (longClickDown == null) {
            longClickDown = new UnityEvent();
        }
        
        if (longClickUp == null) {
            longClickUp = new UnityEvent();
        }
    }

    public void OnClickDown() {
        timeClickStarted = Time.time;
        clicked = false;
        Invoke(nameof(StartLongClick), longClickTime+0.05f);
    }

    public void StartLongClick() {
        if (clicked) {
            return;
        }
        longClickDown.Invoke();
    }

    private void Update() {
        if (!clicked && Input.GetMouseButtonUp(0)) {
            OnClickUp();
        }
    }

    private void OnClickUp() {
        if (Time.time - timeClickStarted < longClickTime) {
            clicked = true;
            shortClickDown.Invoke();
        } else {
            longClickUp.Invoke();
        }
    }
}
