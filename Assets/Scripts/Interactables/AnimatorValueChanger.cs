using System.Collections;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Interactables {
    [RequireComponent(typeof(Animator))]
    public class AnimatorValueChanger  : MonoBehaviour {
        protected Animator animator;
        
        [ValueDropdown(nameof(animatorValues))]
        [SerializeField] private string name;

        private IEnumerable animatorValues() {
            if (animator == null) {
                animator = GetComponent<Animator>();
            }
            return animator.parameters.Select(parameter => parameter.name);
        }
        
        private void Awake() {
            if (animator == null) {
                animator = GetComponent<Animator>();
            }
        }

        public void Trigger() {
            animator.SetTrigger(name);
        }

        public void SetBool(bool value) {
            animator.SetBool(name, value);
        }

        public void ToggleBool() {
            bool val = animator.GetBool(name);
            animator.SetBool(name, !val);
        }

        public void SetFloat(float value) {
            animator.SetFloat(name, value);
        }

        public void SetName(string name) {
            this.name = name;
        }
    }
}