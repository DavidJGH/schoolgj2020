using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Sirenix.Utilities;
using UnityEngine;
using UnityEngine.Events;

namespace Interactables {
    public class Siren : MonoBehaviour {
        [SerializeField] private Transform pedal;

        public float maxSpeed = 6f;
        private float speed = 0f;
        public float lightIntensity = 12f;
        public float sirenActivationScore = 0.5f;

        private bool isRotating = false;

        [SerializeField] private List<Light> lights;

        public UnityEvent OnStartSiren;
        public UnityEvent OnStopSiren;

        private Coroutine _coroutine;

        private Tweener _tweener;

        private void Awake() {
            if (OnStartSiren == null) {
                OnStartSiren = new UnityEvent();
            }

            if (OnStopSiren == null) {
                OnStopSiren = new UnityEvent();
            }
        }

        public void StartRotation() {
            if (!isRotating) {
                DOTween.To(value => speed = value, 0f, maxSpeed, 1f);
                lights.ForEach(light => light.DOIntensity(lightIntensity ,0.3f));
                if (_coroutine != null) {
                    StopCoroutine(_coroutine);
                    _tweener?.Kill();
                }
                
                _coroutine = StartCoroutine(RotationLoop());
                OnStartSiren.Invoke();
            }
        }

        public void OnScoreChange() {
            if (LoseManager.instance.averageResult < sirenActivationScore) {
                StartRotation();
            } else {
                StopRotation();
            }
        }

        public void StopRotation() {
            if (isRotating) {
                _tweener = DOTween.To(value => speed = value, maxSpeed, 0f, 1f).OnComplete(() => {
                    if (_coroutine != null) {
                        StopCoroutine(_coroutine);
                        _coroutine = null;
                    }
                    _tweener = null;
                });
                lights.ForEach(light => light.DOIntensity(0f ,0.3f));
                isRotating = false;
                OnStopSiren.Invoke();
            }
        }

        public void ToggleSiren() {
            if (isRotating) {
                StopRotation();
            } else {
                StartRotation();
            }
        }
        
        IEnumerator RotationLoop() {
            isRotating = true;
            while (true) {
                pedal.Rotate(Vector3.forward, speed, Space.Self);
                yield return null;
            }
        }
    }
}