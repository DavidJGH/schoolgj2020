﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lamp : MonoBehaviour {
    [SerializeField] [ColorUsage(true, true)]
    public Color red;

    [SerializeField] [ColorUsage(true, true)]
    public Color green;

    [SerializeField] private SetEmission setEmission;

    public void MakeRed() {
        setEmission.color = red;
        setEmission.SetEmissive();
        Invoke(nameof(MakeNeutral), 0.25f);
    }

    public void MakeGreen() {
        setEmission.color = green;
        setEmission.SetEmissive();
        Invoke(nameof(MakeNeutral), 0.25f);
    }

    public void MakeNeutral() {
        setEmission.SetUnemissive();
    }
}