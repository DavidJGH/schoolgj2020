﻿using UnityEngine;
using UnityEngine.Events;

public class LoseManager : MonoBehaviour
{
    public static LoseManager instance;

    public UnityEvent OnWrongDecision;
    public UnityEvent OnRightDecision;
    public UnityEvent OnLoseGame;
    public UnityEvent OnScoreChange;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    public float averageResult; //result until this day
    public float correctDecisionsDaily;
    public float wrongDecisionsDaily;
    private float correctDecisions;
    private float wrongDecisions;
    public float worstResultAllowed = 0.3f;
    public float worthOfASecondForResult = 1f;
    public float totalDecisions;
    public float correctPercentage;
    public float boost;
    public float dayResult;
    public int day;
    public int numPeopleLeftInQueue;
    public int targetsFound;

    void Start()
    {
        worthOfASecondForResult = 0.03f;
        averageResult = 0;
        QueueManager.instance.OnQueueDone.AddListener(CheckIfLost);
    }

    public void AddToDecision(bool correct) {
        if (correct) {
            OnRightDecision.Invoke();
            correctDecisions++;
            correctDecisionsDaily++;
        } else {
            OnWrongDecision.Invoke();
            wrongDecisions++;
            wrongDecisionsDaily++;
        }
    }

    public void TimeUpScoreHandler()
    {
        //handle when time is up
        //check how many people are left in the queue
        numPeopleLeftInQueue = QueueManager.instance.numPeopleLeftInQueue();
        Debug.Log("NumPPlLeft: " + numPeopleLeftInQueue);
        QueueManager.instance.timeIsUp();
        //move them somewhere (call some queuemanager method)
        //start NextDay after some time
    }

    public void CheckIfLost()
    {
        PrintStatistics();
        if (averageResult < worstResultAllowed)
        {
            //lost the game
            Debug.Log("YOU LOST!");
            //Actually Lose the game -> Change Screen to Lost
            OnLoseGame.Invoke();
        }
        else
        {
            //live to die another day!
        }
    }

    public void PrintStatistics()
    {
        //this should be displayed on a screen so that you can keep track of how you are doing
        totalDecisions = wrongDecisions + correctDecisions;
        correctPercentage = correctDecisions / totalDecisions;
        boost = ((TimeManager.instance.numSecondsForDay * worthOfASecondForResult) / 100);
        dayResult = (correctDecisions / (wrongDecisions + correctDecisions)) + boost;
        //Debug.Log("---------------------------------");
        //Debug.Log("Day " + day);       
        //Debug.Log("Total Decisions: " + totalDecisions);
        //Debug.Log("Correct Decisions: " + correctDecisions);
        //Debug.Log("Wrong Decisions: " + wrongDecisions);       
        //Debug.Log("Percentage of correct Decisions: " + correctDecisions);
        //Debug.Log("Time left: " + TimeDisplay.instance.minDisplay + ":" + TimeDisplay.instance.secDisplay + " ("+TimeManager.instance.numSecondsForDay+" seconds)");
        //Debug.Log("Overused Time: " + 1); //TODO: implement timer which starts to count once the normal timer is DUNZO and reference it here       
        //Debug.Log("Time boost: " + boost);      
        //Debug.Log("Result of the Day: " + dayResult);
        //Debug.Log("Worst Score allowed: " + worstResultAllowed);
        if (day != 1)
        {
            averageResult = (averageResult + dayResult) / 2;
        }
        else
        {
            averageResult = dayResult;
        }
        OnScoreChange.Invoke();
        //Debug.Log("Cur Total result: " + averageResult);
        //Debug.Log("---------------------------------");
    }
}
