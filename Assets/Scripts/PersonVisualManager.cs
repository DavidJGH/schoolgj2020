﻿using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;

public class PersonVisualManager : SerializedMonoBehaviour
{

        private Animator _animator;

        private Vector3 _lastPos;

        private Transform _child;

        private Quaternion _standardRotation;

        private void Start()
        {
                _animator = transform.GetChild(0).GetComponent<Animator>();
                _child = transform.GetChild(0);
                _standardRotation = _child.rotation;
        }

        private void FixedUpdate()
        {
                var position = transform.position;
                float distance = Vector3.Distance(_lastPos, position);
                _animator.SetFloat("Walkspeed", distance);
                if(distance < float.Epsilon)
                        _child.rotation = _standardRotation; 
                else
                        _child.rotation = Quaternion.LookRotation(_lastPos-position, Vector3.up);
                _lastPos = position;
        }
        
        public Style[] hairstyles;

        public int SetRandomHairstyle(params int[] exclude)
        {
                int index = Random.Range(0, hairstyles.Length);
                while (exclude.Contains(index))
                {
                        index = Random.Range(0, hairstyles.Length);
                }
                SetStyle(hairstyles, index);
                return index;
        }
        
        public Style[] tops;

        public int SetRandomTop(params int[] exclude)
        {
                int index = Random.Range(0, tops.Length);
                while (exclude.Contains(index))
                {
                        index = Random.Range(0, tops.Length);
                }
                SetStyle(tops, index);
                return index;
        }
        
        public Style[] hairMaterials;

        public int SetRandomHairMaterial(params int[] exclude)
        {
                int index = Random.Range(0, hairMaterials.Length);
                while (exclude.Contains(index))
                {
                        index = Random.Range(0, hairMaterials.Length);
                }
                SetStyle(hairMaterials, index);
                return index;
        }
        
        public Style[] outfitMaterials;

        public int SetRandomOutfitMaterial(params int[] exclude)
        {
                int index = Random.Range(0, outfitMaterials.Length);
                while (exclude.Contains(index))
                {
                        index = Random.Range(0, outfitMaterials.Length);
                }
                SetStyle(outfitMaterials, index);
                return index;
        }
        
        public Style[] outerOutfitMaterials;

        public int SetRandomOuterOutfitMaterial(params int[] exclude)
        {
                int index = Random.Range(0, outerOutfitMaterials.Length);
                while (exclude.Contains(index))
                {
                        index = Random.Range(0, outerOutfitMaterials.Length);
                }
                SetStyle(outerOutfitMaterials, index);
                return index;
        }
        
        public Style[] skinMaterials;

        public int SetRandomSkinMaterial(params int[] exclude)
        {
                int index = Random.Range(0, skinMaterials.Length);
                while (exclude.Contains(index))
                {
                        index = Random.Range(0, skinMaterials.Length);
                }
                SetStyle(skinMaterials, index);
                return index;
        }
        
        public Style[] infections;
        
        public int SetRandomInfection()
        {
                int index = Random.Range(0, infections.Length);
                SetStyle(infections, index);
                return index;
        }
        
        public void SetStyle(Style[] styles, int active)
        {
                for (int i = 0; i < styles.Length; i++)
                {
                        styles[i].SetActive(i == active);
                }
        }
}