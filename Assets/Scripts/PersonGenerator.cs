﻿using System;
using System.Collections.Generic;
using Enums;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

public class PersonGenerator : MonoBehaviour
{
    public static PersonGenerator instance;

    private System.Random _random = new System.Random();

    public float probabilityOfInfection = .2f;
    
    public float probabilityOfInvalidPassport = .1f;

    public float probabilityOfGoodLuggage = .2f;

    public float probabilityOfContaminatedLuggage = .2f;

    public int countrySubsetCount = 15;

    public int countrySubsetStart;

    [SerializeField]
    private GameObject malePrefab;
    [SerializeField]
    private GameObject femalePrefab;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

        countrySubsetStart = Random.Range(0, Enum.GetNames(typeof(Countries)).Length - countrySubsetCount);
    }

    public void ChangeSubset()
    {
        countrySubsetStart = Random.Range(0, Enum.GetNames(typeof(Countries)).Length - countrySubsetCount);
    }

    public GameObject CreatePerson(PersonInfo info = null)
    {
        
        info = GeneratePerson(info);

        GameObject personObject;
        
        if (info.gender == true)
        {
            personObject = Instantiate(femalePrefab, QueueManager.instance.QueueSpawnPoint.position, Quaternion.identity);
        }
        else
        {
            personObject = Instantiate(malePrefab, QueueManager.instance.QueueSpawnPoint.position, Quaternion.identity);
        }
        
        Person person = personObject.AddComponent<Person>();

        person.SetAttributes(info);

        return personObject;
    }

    public PersonInfo GeneratePerson(PersonInfo info)
    {
        if(info is null)
            info = new PersonInfo();

        if (info.gender is null)
            info.gender = Random.Range(0, 2) == 0;

        if (info.firstName is null)
        {
            info.firstName = info.gender == true ? 
                ((FemaleNames) Random.Range(0, Enum.GetNames(typeof(FemaleNames)).Length)).ToString("G") : 
                ((MaleNames) Random.Range(0, Enum.GetNames(typeof(MaleNames)).Length)).ToString("G");
        }

        if (info.lastName is null)
            info.lastName = ((LastNames) Random.Range(0, Enum.GetNames(typeof(LastNames)).Length)).ToString("G");
        
        if (info.country is null)
            info.country = ((Countries) Random.Range(countrySubsetStart, countrySubsetStart + countrySubsetCount));

        if(info.birthdate is null)
            info.birthdate = DateBetweenYears(-80, -18);
        
        if(info.expiration is null)
            info.expiration = DateBetweenYears(-3, 30);

        if (Random.value <= probabilityOfInfection)
        {
            info.flags.Add(Flags.Infected);
        }
        if(Random.value <= probabilityOfGoodLuggage)
        {
            info.flags.Add(Flags.hasGoodLuggage);
        }
        if(Random.value <= probabilityOfContaminatedLuggage)
        {
            //if contaminated luggage -> no good luggage
            info.flags.Add(Flags.hasContaminatedLuggage);
            if (info.flags.Contains(Flags.hasGoodLuggage))
                info.flags.Remove(Flags.hasGoodLuggage);
        }
        if(Random.value <= probabilityOfInvalidPassport)
        {
            info.flags.Add(Flags.InvalidPassport);
        }

        return info;
    }

    //this is relative to current time
    public DateTime DateBetweenYears(int from, int to)
    {
        DateTime start = DateTime.Today.AddYears(from);
        int range = (DateTime.Today.AddYears(to) - start).Days;
        return start.AddDays(_random.Next(range));
    }
}