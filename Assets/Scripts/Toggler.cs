﻿using UnityEngine;
using UnityEngine.Events;

public class Toggler : MonoBehaviour {
    private int flicks;

    public UnityEvent OnEven;
    public UnityEvent OnOdd;

    public void Toggle() {
        flicks++;
        if (flicks%2==0) {
            OnEven.Invoke();
        } else {
            OnOdd.Invoke();
        }
    }
}
