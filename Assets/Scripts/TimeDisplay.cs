﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TimeDisplay : MonoBehaviour
{
    public static TimeDisplay instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    public TextMeshPro minuteTimer;
    public TextMeshPro secondTimer;
    public string minDisplay;
    public string secDisplay;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        minDisplay = TimeManager.instance.numMinutes.ToString();
        if (TimeManager.instance.numMinutes / 10 < 1)
            minDisplay = "0" + TimeManager.instance.numMinutes;
        minuteTimer.SetText(minDisplay);
        secDisplay = TimeManager.instance.numSeconds.ToString();
        if (TimeManager.instance.numSeconds / 10 < 1)
            secDisplay = "0" + TimeManager.instance.numSeconds;
        secondTimer.SetText(secDisplay);
    }
}
