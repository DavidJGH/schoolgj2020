using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityTemplateProjects;
using UnityEngine.Events;

namespace UnityTemplateProjects {
    public class GameManager : MonoBehaviour {
        public static GameManager instance;
        public int day;
        public List<GameObject> peopleToQueue;
        public float delay;

        public bool doneWithDay;

        public UnityEvent OnShowScore;
        public UnityEvent OnHideScore;

        public bool lostGame;

        private void Awake() {
            if (instance == null) {
                instance = this;
            }
        }


        private void Start() {
            day = 0;
            QueueManager.instance.delay = 1f;
            delay = 0.2f;
            QueueManager.instance.OnQueueDone.AddListener(ShowScore);
            doneWithDay = true;
            SetupNextDay();
            QueueManager.instance.delay = 0.3f;
        }

        void ShowScore() {
            doneWithDay = true;
            OnShowScore.Invoke();
        }

        public void SetupNextDay() {
            if (doneWithDay) {
                TimeManager.instance.ToggleClock();
                OnHideScore.Invoke();
                day++;
                doneWithDay = false;
                //set time for day 
                Debug.Log("Start new Day");
                LoseManager.instance.day = day;
                TimeManager.instance.day = day;
                TimeManager.instance.ResetTimeDay();
                //reset variables
                peopleToQueue = new List<GameObject>();
                QueueManager.instance.queuePosition = -1;
                PersonVisualiser.instance.ClearExclusions();
                QueueManager.instance.targetNotificationTiming.Clear();
                TimeManager.instance.notInvokedYet = true;
                //workflow
                //num of targets -> create first and exclude their stuff from normal people
                int numTargetsThisDay = Random.Range(1, 3);
                if (day <= 1) {
                    numTargetsThisDay = 0;
                }

                //create Targets
                for (int i = 0; i < numTargetsThisDay; i++) {
                    PersonInfo personInfoWithTargetFlag = new PersonInfo();
                    personInfoWithTargetFlag.flags.Add(Flags.Target);
                    GameObject personObject = PersonGenerator.instance.CreatePerson(personInfoWithTargetFlag);
                    personObject.GetComponent<Person>().description =
                        PersonVisualiser.instance.Visualise(personObject.GetComponent<Person>(), true);
                    peopleToQueue.Add(personObject);
                }

                //non targets -> create them after the targets
                int numNormalPersonsThisDay = (day * 5) - numTargetsThisDay;
                //spawn num of person depending on day num
                for (int i = 0; i < numNormalPersonsThisDay; i++) {
                    GameObject personObject = PersonGenerator.instance.CreatePerson();
                    PersonVisualiser.instance.Visualise(personObject.GetComponent<Person>());
                    peopleToQueue.Add(personObject);
                }

                //TODO: queue all people in random order and set the SendTargetInfo to a random number before the index of the person
                SetupQueue();
                Rulebook.instance.newRuleSet(day);
                QueueManager.instance.NextPerson();
                //time is working setupwise
                Debug.Log(TimeManager.instance.numSecondsForDay);
            }

            void SetupQueue() {
                //create array
                //add person from list to a random array index if that index is null
                //if that index is not null try a different number
                //if Target Flag of PersonInfo of Person of Gameobject is set -> store the sendInfoBeforeQueueNumber value
                //cast back to list and add all of them to the queue
                GameObject[] arrayOfPplToQueue = peopleToQueue.ToArray();
                GameObject[] actualOrder = new GameObject[arrayOfPplToQueue.Length];
                List<GameObject> queue = new List<GameObject>();
                List<int> excludeFromRandom = new List<int>();
                for (int i = 0; i < arrayOfPplToQueue.Length; i++) {
                    //Debug.Log(i);
                    excludeFromRandom = AddIntoArray(arrayOfPplToQueue, actualOrder, i, excludeFromRandom);
                }

                //cast into queue list
                for (int i = 0; i < actualOrder.Length; i++) {
                    if (actualOrder[i] != null)
                        queue.Add(actualOrder[i]);
                }

                //add into queue
                foreach (GameObject person in queue) {
                    QueueManager.instance.queuedPeople.Add(person);
                }
            }
        }

        List<int> AddIntoArray(GameObject[] arrayOfPplToQueue, GameObject[] actualOrder, int i,
            List<int> excludeFromRandom) {
            int queuePosition = GetValidRandom(0, arrayOfPplToQueue.Length, excludeFromRandom);
            if (actualOrder[queuePosition] == null) {
                //no person at this queue position
                actualOrder[queuePosition] = arrayOfPplToQueue[i];
                //check if person is target and set some variable for him that says when the clue has to array at last
                if (actualOrder[queuePosition].GetComponent<Person>().flags.Contains(Flags.Target)) {
                    //TODO: SET THE VARIABLE FOR CLUE TIMING
                    QueueManager.instance.targetNotificationTiming.Add(new TargetQueueInfo
                    (actualOrder[queuePosition].GetComponent<Person>(),
                        queuePosition));
                }

                //excludeFromFutureRandoms
                excludeFromRandom.Add(queuePosition);
            } else {
                //already a person at this queue position

                Debug.Log("Error!");
            }

            return excludeFromRandom;
        }

        int GetValidRandom(int lowestNumIncluded, int highestNumExluded, List<int> exclusions) {
            List<int> validChoices = new List<int>();
            int num = -1; //placeholder
            for (int i = 0; i < highestNumExluded; i++) {
                bool dontAdd = false;
                foreach (int exclusion in exclusions) {
                    if (i == exclusion) {
                        dontAdd = true;
                    }
                }

                if (!dontAdd) {
                    //add it to valid choices
                    validChoices.Add(i);
                }
            }

            int[] validChoicesArray = validChoices.ToArray();
            num = validChoicesArray[Random.Range(0, validChoicesArray.Length)];
            return num;
        }
    }
}