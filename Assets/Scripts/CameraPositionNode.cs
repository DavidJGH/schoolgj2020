﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class CameraPositionNode : MonoBehaviour {
    [SerializeField]
    public bool customFOV;

    [ShowIf(nameof(customFOV))]
    public float fov;
    public float transitionTime = 0.5f;
    
    public void ToggleMove() {
        if (CameraPositionManager.instance.currentNode==this) {
            TransitionBack();
        } else {
            TransitionTo();
        }
    }
    
    public void TransitionTo() {
        CameraPositionManager.instance.MoveToNode(this);
    }

    public void TransitionBack() {
        CameraPositionManager.instance.MoveBack();
    }
}