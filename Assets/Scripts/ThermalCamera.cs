﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Camera))]
public class ThermalCamera : MonoBehaviour
{
    public Shader shader;
    public Shader replaceInMain;

    private Camera camera;

    public bool active;

    public UnityEvent OnSetActive;
    public UnityEvent OnSetInactive;

    private void Awake() {
        camera = GetComponent<Camera>();
        camera.SetReplacementShader(replaceInMain, "");
    }

    public void Toggle() {
        if (active) {
            Disable();
        } else {
            Enable();
        }
    }
    
    [Button]
    public void Enable() {
        active = true;
        camera.SetReplacementShader(shader, "");
        OnSetActive.Invoke();
    }
    
    [Button]
    public void Disable()
    {
        active = false;
        camera.SetReplacementShader(replaceInMain, "");
        OnSetInactive.Invoke();
    }
}
