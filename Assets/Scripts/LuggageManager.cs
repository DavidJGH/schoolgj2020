﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LuggageManager : MonoBehaviour
{
    public static LuggageManager instance;
    public Transform luggageSpawnPosition;

    [SerializeField]
    public List<GameObject> goodLuggages = new List<GameObject>();
    [SerializeField]
    public List<GameObject> badLuggages = new List<GameObject>();

    public GameObject activeLuggage;
    Animator animator;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    void Update()
    {
    }

    private void Start()
    {
        QueueManager.instance.OnNewActivePerson.AddListener(HandleLuggage);
        PlayerController.instance.OnDecisionMade.AddListener(FinishLuggageAnimation);
    }

    public void HandleLuggage()
    {
        Person activePerson = QueueManager.instance.activePerson;
        //check if has luggage
        if (activePerson.flags.Contains(Flags.hasGoodLuggage))
        {
            //spawn a good luggage
            activeLuggage = Instantiate(goodLuggages[Random.Range(0, goodLuggages.Count)], luggageSpawnPosition.position, Quaternion.Euler(-90,0,0));

        }
        else if (activePerson.flags.Contains(Flags.hasContaminatedLuggage))
        {
            //spawn a bad luggage
            activeLuggage = Instantiate(badLuggages[Random.Range(0, goodLuggages.Count)], luggageSpawnPosition.position, Quaternion.Euler(-90, 0, 0));
        }
        if(activeLuggage != null)
        {
            animator = activeLuggage.GetComponent<Animator>();
        }
    }

    public void FinishLuggageAnimation()
    {
        if (activeLuggage != null)
        {
            activeLuggage.GetComponent<Animator>().SetBool("leaveScanner", true);
        }
        activeLuggage = null;
    }
}
