﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Rules;
using Sirenix.Utilities;
using TMPro;
using UnityEngine;

public class RulesTextManager : MonoBehaviour
{
    public TextMeshPro text;
    
    void Awake()
    {
        Rulebook.instance.onRulesChanged.AddListener(RulesUpdated);
    }

    void RulesUpdated()
    {
        String targets = "";
        if(Rulebook.instance.activeRules.Count(e => e is CountryRule) > 0)
            ((CountryRule)Rulebook.instance.activeRules.First(e => e is CountryRule)).countries.Select(e => e.ToString("G").Replace('_',' ')+"\n\n").ForEach(s => targets += s);
        text.text = targets;
    }
}
