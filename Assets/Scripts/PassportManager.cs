﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public class PassportManager : MonoBehaviour
{
    public TextMeshPro passport;
    public SpriteRenderer watermark;
    public SpriteRenderer portrait;
    
    public Sprite invalidPortrait;
    private Sprite original;

    public float invalidPortraitProbability = 0.02f;
    
    void Awake()
    {
        original = portrait.sprite;
        QueueManager.instance.OnNewActivePerson.AddListener(NewActivePerson);
    }

    void NewActivePerson()
    {
        Person active = QueueManager.instance.activePerson;
        
        watermark.enabled = true;
        portrait.sprite = original;
        portrait.color = Color.black;


        if (active.flags.Contains(Flags.InvalidPassport))
        {
            if (Random.value <= invalidPortraitProbability)
            {
                portrait.sprite = invalidPortrait;
                portrait.color = Color.white;
                active.lastName = "SPEED";
                active.firstName = "Green";
                active.birthdate = new DateTime(1974, 12, 18);
            }
            else
            {
                watermark.enabled = false;
            }
        }

        passport.text = "<size=2><color=#664764>Surname\n" +
                        "<size=3><color=#000000>" + active.lastName + "\n" +
                        "<size=2><color=#664764>Given names\n" +
                        "<size=3><color=#000000>" + active.firstName + "\n" +
                        "<size=2><color=#664764>Nationality\n" +
                        "<size=3><color=#000000>" + active.country.ToString("G").Replace('_', ' ') + "\n" +
                        "<size=2><color=#664764>Date of Birth\n" +
                        "<size=3><color=#000000>" + active.birthdate.ToString("d MM yyyy") + "\n" +
                        "<size=2><color=#664764>Sex\n" +
                        "<size=3><color=#000000>" + (active.gender ? "F" : "M") + "\n" +
                        "<size=2><color=#664764>Date of issue\n" +
                        "<size=3><color=#000000>1 5 2017\n" +
                        "<size=2><color=#664764>Date of expiry\n" +
                        "<size=3><color=#000000>" + active.expiration.ToString("d MM yyyy") + "\n";
    }
}
