﻿using System;

[Serializable]
public abstract class Rule 
{
        //Returns true when person is to be sorted out
        public abstract bool AppliesTo(Person person);
}