﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.Utilities;
using TMPro;
using UnityEngine;

public class TargetTextManager : MonoBehaviour
{
    public TextMeshPro text;
    
    void Awake()
    {
        QueueManager.instance.TargetNotification.AddListener(PersonRevealedUpdated);
        QueueManager.instance.OnQueueDone.AddListener(ClearTargets);
    }

    void PersonRevealedUpdated()
    {
        String targets = "";
        QueueManager.instance.targetNotificationTiming.Where(e => e.wasNotified)
            .Select(e => e.targetPerson.description + "\n\n").ForEach(s => targets += s);
        text.text = targets;
    }

    void ClearTargets()
    {
        text.text = "";
    }
}
