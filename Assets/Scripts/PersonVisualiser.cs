﻿using System;
using Boo.Lang;
using UnityEngine;
using Random = UnityEngine.Random;

public class PersonVisualiser : MonoBehaviour
{
    private int _numberOfAttributes = 4;
    public List<int[]> exclusions;
    
    public static PersonVisualiser instance;
    private void Awake() {
        if (instance == null) {
            instance = this;
        }
        exclusions = new List<int[]>();
    }
    
    public string Visualise(Person person, bool addToExclusions = false)
    {
        PersonVisualManager visuals = person.GetComponent<PersonVisualManager>();

        //this is for addToExclusions
        int excludeArticle = Random.Range(0, _numberOfAttributes);
        int[] personExclusions = new int[_numberOfAttributes];
        
        int style = visuals.SetRandomHairstyle(GetExclusions(0));
        if(addToExclusions)
            personExclusions[0] = excludeArticle == 0 ? style : -1;
        
        style = visuals.SetRandomHairMaterial(GetExclusions(1));
        if(addToExclusions)
            personExclusions[1] = excludeArticle == 1 ? style : -1;
        
        style = visuals.SetRandomOutfitMaterial(GetExclusions(2));
        if(addToExclusions)
            personExclusions[2] = excludeArticle == 2 ? style : -1;
        
        style = visuals.SetRandomOuterOutfitMaterial(GetExclusions(3));
        if(addToExclusions)
            personExclusions[3] = excludeArticle == 3 ? style : -1;

        visuals.SetRandomTop();

        visuals.SetRandomSkinMaterial();
        
        if (person.flags.Contains(Flags.Infected))
        {
            visuals.SetRandomInfection();
        }

        if(addToExclusions)
            exclusions.Add(personExclusions);

        return person.firstName + " " + person.lastName;
    }

    public int[] GetExclusions(int index)
    {
        List<int> exclude = new List<int>();
        foreach (var exclusion in exclusions)
        {
            if (exclusion[index] != -1)
                exclude.Add(exclusion[index]);
        }

        return exclude.ToArray();
    }

    public void ClearExclusions()
    {
        exclusions = new List<int[]>();
    }
}