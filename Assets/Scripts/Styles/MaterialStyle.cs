﻿using System;
using Boo.Lang;
using Sirenix.Utilities;
using UnityEngine;

namespace Styles
{
    public class MaterialStyle : Style
    {
        public List<Tuple<SkinnedMeshRenderer, int>> renderer = new List<Tuple<SkinnedMeshRenderer, int>>();
        public Material material;
        
        public MaterialStyle(string description, Material material) : base(description)
        {
            this.material = material;
        }

        public override void SetActive(bool active)
        {
            if (active)
                renderer.ForEach(e =>
                {
                    var mats = e.Item1.materials;
                    mats[e.Item2] = material;
                    e.Item1.materials = mats;
                });
        }
    }
}