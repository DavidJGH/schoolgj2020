﻿using UnityEngine;

namespace Styles
{
    public class GameObjectStyle : Style
    {
        public GameObject gameObject;
        
        public GameObjectStyle(string description, GameObject gameObject) : base(description)
        {
            this.gameObject = gameObject;
        }

        public override void SetActive(bool active)
        {
            gameObject.SetActive(active);
        }
    }
}