﻿namespace Styles
{
    public class NoStyle : Style
    {
        public NoStyle(string description) : base(description)
        {
        }

        public override void SetActive(bool active)
        {
        }
    }
}