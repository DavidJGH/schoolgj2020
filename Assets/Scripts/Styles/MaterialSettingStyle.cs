﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialSettingStyle : Style
{
    public List<Tuple<SkinnedMeshRenderer, int>> renderer = new List<Tuple<SkinnedMeshRenderer, int>>();

    public String setting;

    public float value;
    
    public MaterialSettingStyle(string description, string setting, float value) : base(description)
    {
        this.setting = setting;
        this.value = value;
    }

    public override void SetActive(bool active)
    {
        if (active)
            renderer.ForEach(e =>
            {
                var mats = e.Item1.materials;
                mats[e.Item2].SetFloat(setting, value);
                e.Item1.materials = mats;
            });
    }
}
