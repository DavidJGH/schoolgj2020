﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using DG.Tweening;
using UnityEngine;

public class CameraPositionManager : MonoBehaviour {
    public static CameraPositionManager instance;
    [HideInInspector]
    public Camera camera;

    private Vector3 defaultPos;
    private float defaultFOV;
    private Quaternion defaultRotation;

    public CameraPositionNode currentNode;
    
    private void Awake() {
        if (instance == null) {
            instance = this;
        }

        camera = Camera.main;

        defaultFOV = camera.fieldOfView;
        defaultRotation = camera.transform.rotation;
        defaultPos = camera.transform.position;
    }

    public void MoveToNode(CameraPositionNode node) {
        MoveToPos(node.transform.position, node.transitionTime, node.transform.rotation, node.customFOV?node.fov:(float?) null);
        currentNode = node;
    }
    
    private void MoveToPos(Vector3 position, float transitionTime, Quaternion? rotation = null, float? fov = null) {
        camera.transform.DOMove(position, transitionTime);
        if (rotation.HasValue) {
            camera.transform.DORotate(rotation.Value.eulerAngles, transitionTime);
        }
        camera.DOFieldOfView(fov ?? defaultFOV, transitionTime);

    }

    public void MoveBack() {
        currentNode = null;
        MoveToPos(defaultPos,0.5f, defaultRotation, defaultFOV);
    }
    
    
}
