﻿using Rules;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerController : MonoBehaviour {
    public static PlayerController instance;
    public UnityEvent OnDecisionMade;

    private void Awake() {
        if (instance == null) {
            instance = this;
        }
    }

    //STUPID UNTIY DOESNT SHOW ENUMS IN EVENTS
    public void LetPass() {
        MakeDecision(Decision.LetPass);
    }

    public void DenyEntry() {
        MakeDecision(Decision.DenyEntry);
    }

    public void IsTarget() {
        MakeDecision(Decision.IsTarget);
    }

    public void IsInfected() {
        MakeDecision(Decision.IsInfected);
    }

    public void MakeDecision(Decision decision) //0 is sort out, 1 is let him enter, 2 is a target
    {
        OnDecisionMade.Invoke();
        List<Rule> rulesThatWereApplied = new List<Rule>();
        List<Flags> flagsThatWereApplied = new List<Flags>();
        bool correctDecision;
        //if want to sort out -> sortOut is true
        Person activePerson = QueueManager.instance.activePerson;
        bool sortOut = (decision != Decision.LetPass);
        QueueManager.instance.decision = decision;

        if (activePerson != null) {
            switch (decision) {
                case Decision.DenyEntry:
                    //sort out
                    //TODO: not sure about this code seems really weird, I already refactored it a bit but its not 100 percent clear yet
                    correctDecision = Rulebook.instance.CheckPersonIsDenied(activePerson, ref rulesThatWereApplied, ref flagsThatWereApplied);
                    LoseManager.instance.AddToDecision(correctDecision);
                    if (!correctDecision)
                    {
                        Debug.Log("You made a mistake at person: " + activePerson.firstName);
                        foreach (Rule rule in rulesThatWereApplied)
                        {
                            if (rule is CountryRule)
                            {
                                rulesThatWereApplied.Remove(rule);
                            }
                        }
                        foreach (Flags flag in (Flags[])System.Enum.GetValues(typeof(Flags)))
                        {
                            if (flag is Flags.hasContaminatedLuggage || flag is Flags.InvalidPassport)
                            {
                                flagsThatWereApplied.Remove(flag);
                            }
                        }
                        FeedbackManager.instance.GiveFeedback(rulesThatWereApplied, flagsThatWereApplied);
                    }
                    break;
                case Decision.LetPass:
                    //is oke
                    
                    correctDecision = Rulebook.instance.CheckPersonMayPass(activePerson, ref rulesThatWereApplied, ref flagsThatWereApplied);
                    LoseManager.instance.AddToDecision(correctDecision);
                    if (!correctDecision)
                    {
                        Debug.Log("You made a mistake at person: " + activePerson.firstName);
                       
                        FeedbackManager.instance.GiveFeedback(rulesThatWereApplied, flagsThatWereApplied);
                    }
                    break;
                case Decision.IsTarget:
                    //it is a target -> different score values as well as different animation(Trapdoor kill)
                    //sort out

                    correctDecision = Rulebook.instance.CheckPersonIsTarget(activePerson, ref rulesThatWereApplied, ref flagsThatWereApplied);
                    LoseManager.instance.AddToDecision(correctDecision);
                    if (!correctDecision)
                    {
                        Debug.Log("You made a mistake at person: " + activePerson.firstName);
                        foreach (Flags flag in (Flags[])System.Enum.GetValues(typeof(Flags)))
                        {
                            if (flag is Flags.Target)
                            {
                                flagsThatWereApplied.Remove(flag);
                            }
                        }
                        FeedbackManager.instance.GiveFeedback(rulesThatWereApplied, flagsThatWereApplied);
                    }
                    break;
                case Decision.IsInfected:
                    //person is infected -> different score values as well as different animation
                    //sort out
                    correctDecision = Rulebook.instance.CheckPersonIsInfected(activePerson, ref rulesThatWereApplied, ref flagsThatWereApplied);
                    LoseManager.instance.AddToDecision(correctDecision);
                    if (!correctDecision)
                    {
                        Debug.Log("You made a mistake at person: " + activePerson.firstName);
                        //exclude the rules and flags from this decision
                        foreach (Flags flag in (Flags[])System.Enum.GetValues(typeof(Flags)))
                        {
                            if(flag is Flags.Infected)
                            {
                                flagsThatWereApplied.Remove(flag);
                            }
                        }
                            //give feedback why you were wrong
                            FeedbackManager.instance.GiveFeedback(rulesThatWereApplied, flagsThatWereApplied);
                    }
                    break;
            }

            QueueManager.instance.NextPerson();
        }
    }
}

public enum Decision {
    DenyEntry = 0,
    LetPass = 1,
    IsTarget = 2,
    IsInfected = 3
}