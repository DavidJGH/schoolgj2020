﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighScoreManager : MonoBehaviour
{
    public static HighScoreManager instance;
    public float score;
    public float highscore;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        LoseManager.instance.OnLoseGame.AddListener(TryToSetHighscore);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TryToSetHighscore()
    {
        score = LoseManager.instance.averageResult;
        if(highscore > score)
        {
            //set highscore
        }
    }
}
