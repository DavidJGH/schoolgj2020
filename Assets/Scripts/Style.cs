﻿using System;
using Sirenix.Serialization;

[Serializable]
public abstract class Style
{
    public string description;

    public Style(string description)
    {
        this.description = description;
    }

    public abstract void SetActive(bool active);
}