﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityTemplateProjects;

public class TimeManager : MonoBehaviour
{

    public static TimeManager instance;
    public UnityEvent OnTimeUp;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    private void Start() {
        timerIsActive = true;
        notInvokedYet = true;
        QueueManager.instance.OnQueueDone.AddListener(ResetTimeDay);
        QueueManager.instance.OnQueueDone.AddListener(ToggleClock);
        LoseManager.instance.OnLoseGame.AddListener(StopClock);
    }

    public bool timerIsActive;
    public float numSecondsForDay; 
    public int numSeconds;
    public int numMinutes;
    public bool notInvokedYet;
    public int day;

    public void StopClock()
    {
        timerIsActive = false;
    }

    public void ToggleClock() {
        timerIsActive = !timerIsActive;
    }

    void Update()
    {
        if (numSecondsForDay > 0)
        {
            if (timerIsActive) {
                numSecondsForDay -= Time.deltaTime;
                numMinutes = Mathf.FloorToInt(numSecondsForDay / 60);
                numSeconds = Mathf.FloorToInt(numSecondsForDay - (numMinutes * 60));
                //set text to min : sec
            }
        }
        else if (numSecondsForDay < 0 && notInvokedYet)
        {
            Debug.Log("Time up");
            OnTimeUp.Invoke();
            notInvokedYet = false;
            ResetTimeDay();
        }
    }

    public void ResetTimeDay()
    {
        numSecondsForDay = 120 + (day*60);
    }

    public void SetDayTimer(int seconds)
    {
        numSecondsForDay = seconds;
    }
}
