﻿Shader "Custom/VHSShader"
{
    Properties
    {
        [HDR]_Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _NoiseTex ("Noise", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
        _Noise ("Noise", Range(0,1)) = .04
        _ScanLineBig ("Big Scan Lines", Range(0,1)) = .15
        _ScanOffset ("Scan Line offset", Range(0,5)) = 1
        _GlitchIntensityH ("Glitch Intensity Horizontal", Range(0,1)) = .15
        _GlitchIntensityV ("Glitch Intensity Vertical", Range(0,1)) = .02
        _TimeMultiplier ("Time Multiplier", Range(0,5)) = 1
        _RandomSeed ("Seed", Range(0,10)) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;
        sampler2D _NoiseTex;
        half _TimeMultiplier;
        half _RandomSeed;

        struct Input
        {
            float2 uv_MainTex;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        float onOff(float a, float b, float c)
        {
        	return step(c, sin(_Time[1] + a*cos(_Time[1]*b)));
        }
        
        float noise(fixed2 p)
        {
        	float s = tex2D(_NoiseTex,fixed2(1.,2.*cos(_Time[1]*_TimeMultiplier))*_Time[1]*_TimeMultiplier*8. + p*1.).x;
        	s *= s;
        	return s;
        }
        
        half _GlitchIntensityH;
        half _GlitchIntensityV;
        
        fixed3 getVideo(fixed2 uv)
        {
        	fixed2 look = uv;
        	float window = 1./(1.+20.*(look.y-fmod(_Time[1]/4.,1.))*(look.y-fmod(_Time[1]/4.,1.)));
        	look.x = look.x + sin(look.y*10. + _Time[1])/50.*onOff(4.,.4,.3)*_GlitchIntensityH*(1.+cos(_Time[1]*80.))*window;
        	float vShift = _GlitchIntensityV*onOff(.5,.5,2.)*(sin(_Time[1])*sin(_Time[1]*20.) + 
        										 (0.5 + 0.1*sin(_Time[1]*200.)*cos(_Time[1])));
        	look.y = fmod(look.y + vShift, 1.);
        	fixed3 video = tex2D (_MainTex,look);
        	return video;
        }
        
        float ramp(float y, float start, float end)
        {
        	float inside = step(start,y) - step(end,y);
        	float fact = (y-start)/(end-start)*inside;
        	return (1.-fact) * inside;
        	
        }
        
        half _ScanOffset;
        
        float stripes(fixed2 uv)
        {
        	
        	float noi = noise(uv*fixed2(0.5,1.) + fixed2(1.,3.));
        	return ramp(fmod(uv.y*4.*_ScanOffset + (_Time[1]+_RandomSeed)*_TimeMultiplier/2.+sin(_Time[1]*_TimeMultiplier + sin((_Time[1]+_RandomSeed)*_TimeMultiplier*0.63)),1.),0.5,0.6)*noi;
        }
        
        fixed2 screenDistort(fixed2 uv)
        {
        	uv -= fixed2(.5,.5);
        	uv = uv*1.2*(1./1.2+2.*uv.x*uv.x*uv.y*uv.y);
        	uv += fixed2(.5,.5);
        	return uv;
        }

        half _Noise;
        half _ScanLineBig;

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            // Albedo comes from a texture tinted by color
            float2 uv = IN.uv_MainTex;


	        fixed3 video = getVideo(uv);
            uv = screenDistort(uv);
            float vigAmt = 3.+.3*sin(_Time[1]*_TimeMultiplier + 5.*cos(_Time[1]*_TimeMultiplier*.1));
	        float vignette = (1.-vigAmt*(uv.y-.5)*(uv.y-.5))*(1.-vigAmt*(uv.x-.5)*(uv.x-.5));
        
	        video *= (12.+fmod(uv.y*30.+_Time[1]*_TimeMultiplier,1.))/13.;
            video += noise(uv*3.)*_Noise;
	        video += stripes(uv)*_ScanLineBig;
	        video *= vignette*1.1;
            
            o.Albedo = video * _Color;
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = 1;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
