﻿Shader "Unlit/StandardThermal"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Temperature ("Temperature", Range(22, 40)) = 0   
        /*
        _ColorTop ("Top Color", Color) = (0, 0.11, 0.52, 1)
        _ColorMidTop ("MidTop Color", Color) = (0.13, 0.75, 0.19, 1)
        _ColorMidBot ("MidBot Color", Color) = (1, 0.89, 0.07, 1)
        _ColorBot ("Bot Color", Color) = (1, 0.18, 0, 1)
        _MiddleBot ("MiddleBot", Range(0.001, 0.999)) = 0.33
        _MiddleTop ("MiddleTop", Range(0.001, 0.999)) = 0.66*/
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float dist : TEXCOORD1;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            half _Temperature;
            
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.dist = (length(ObjSpaceViewDir(v.vertex))/5);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 _ColorTop = fixed4(0, 0.11, 0.52, 1);
                fixed4 _ColorMidTop = fixed4(0, .27, 1, 1);
                fixed4 _ColorMidBot = fixed4(1, 0.53, 0, 1);
                fixed4 _ColorBot = fixed4(1, 0, 0, 1);
                float _MiddleBot = .33;
                float _MiddleTop = .66;
                float f = 1-((max(_Temperature,22) - 22)/(40-22));
                fixed4 c = lerp(_ColorBot, _ColorMidBot, f / _MiddleBot) * step(f, _MiddleBot);
                c += lerp(_ColorMidBot, _ColorMidTop, (f - _MiddleBot) /(_MiddleTop - _MiddleBot) ) * step(_MiddleBot,f) * step(f,_MiddleTop);
                c += lerp(_ColorMidTop, _ColorTop, (f - _MiddleTop) / (1 - _MiddleTop)) * step(_MiddleTop, f);
                c.a = 1;
                c /= i.dist;
                return c;
            }
            ENDCG
        }
    }
}
