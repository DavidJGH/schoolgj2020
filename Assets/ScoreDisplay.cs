﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreDisplay : MonoBehaviour
{
    public static ScoreDisplay instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    public TextMeshPro dailyScoreDisplay;
    public TextMeshPro totalScoreDisplay;
    public TextMeshPro dayDisplay;

    // Start is called before the first frame update
    void Start()
    {
        QueueManager.instance.OnQueueDone.AddListener(JustDisplay);
        TimeManager.instance.OnTimeUp.AddListener(DisplayScoreInfo);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DisplayScoreInfo()
    {
        LoseManager.instance.TimeUpScoreHandler();
        JustDisplay();
    }

    public void JustDisplay()
    {

        float dayResult = LoseManager.instance.dayResult;
        float totalResult = LoseManager.instance.averageResult;
        dayDisplay.text = "Day " + TimeManager.instance.day;
        dailyScoreDisplay.text = 
            "Decisions made: " + ((int)(LoseManager.instance.wrongDecisionsDaily + LoseManager.instance.correctDecisionsDaily)).ToString() + "\n" +
            "Correct Decisions: " + ((int)LoseManager.instance.correctDecisionsDaily).ToString() + "\n" +
            "Wrong Decisions: " + ((int)LoseManager.instance.wrongDecisionsDaily).ToString() + "\n" +
            "People not handled: " + ((int)LoseManager.instance.numPeopleLeftInQueue).ToString() + "\n" +
            "Targets found: " + ((int)LoseManager.instance.targetsFound).ToString() + "\n" +
            "Time left : " + TimeDisplay.instance.minDisplay + ":" + TimeDisplay.instance.secDisplay + " (" + TimeManager.instance.numSecondsForDay + " seconds)" + "\n" + 
            "Day Percentage: " + Mathf.Round(dayResult*100).ToString() + "%\n";
        totalScoreDisplay.text = "Total Percentage: " + Mathf.Round(totalResult * 100).ToString() + "%";
    }
}
